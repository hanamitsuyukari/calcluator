package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Calc calc = new Calc();
    private EditText display;
    private Button[] buttons;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        display = findViewById(R.id.editText);

        buttons = new Button[17];
        int[] buttonId = {R.id.buttonZero,R.id.button1,R.id.button2,R.id.button3,R.id.button4,
                            R.id.button5,R.id.button6,R.id.button7,R.id.button8,R.id.button9,R.id.buttonPls,
                            R.id.buttonMinus,R.id.buttonMul,R.id.buttonDiv,R.id.buttonDot,R.id.buttonEqual,R.id.buttonAC};
        for(int i = 0;i < buttonId.length;i++){
            buttons[i] = findViewById(buttonId[i]);
            buttons[i].setOnClickListener(this);
        }

        calc = new Calc("0","0");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonZero:
                display.setText(calc.inputData("0"));
                break;
            case R.id.button1:
                display.setText(calc.inputData("1"));
                break;
            case R.id.button2:
                display.setText(calc.inputData("2"));
                break;
            case R.id.button3:
                display.setText(calc.inputData("3"));
                break;
            case R.id.button4:
                display.setText(calc.inputData("4"));
                break;
            case R.id.button5:
                display.setText(calc.inputData("5"));
                break;
            case R.id.button6:
                display.setText(calc.inputData("6"));
                break;
            case R.id.button7:
                display.setText(calc.inputData("7"));
                break;
            case R.id.button8:
                display.setText(calc.inputData("8"));
                break;
            case R.id.button9:
                display.setText(calc.inputData("9"));
                break;
            case R.id.buttonDot:
                display.setText(calc.inputData("."));
                break;

            case R.id.buttonMul:
                calc.onOpeButtonClick(1);
                break;
            case R.id.buttonDiv:
                calc.onOpeButtonClick(2);
                break;
            case R.id.buttonPls:
                calc.onOpeButtonClick(3);
                break;
            case R.id.buttonMinus:
                calc.onOpeButtonClick(4);
                break;
            case R.id.buttonEqual:
                display.setText(calc.doCalc());
                break;
            case R.id.buttonAC:
                display.setText(calc.clear());
                break;

        }

    }
}
